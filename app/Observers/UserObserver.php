<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;

class UserObserver
{
    public function updated(User $user)
    {
        if ($user->isDirty('email')) {
            $user->email_verified_at = null;
            $user->sendEmailVerificationNotification();
        }

        if ($user->isDirty('password')) {
            $currentToken = $user->currentAccessToken();
            $user->tokens()->whereNotIn('id', $currentToken->id)->delete();
            event(new PasswordReset($user));
        }
        $user->saveQuietly();
    }
}
