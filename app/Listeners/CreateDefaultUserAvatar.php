<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Jdenticon\Identicon;

class CreateDefaultUserAvatar implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Verified $event
     * @return void
     */
    public function handle(Verified $event)
    {
        $user = $event->user;
        $icon = new Identicon();
        $icon->setValue(collect($user));
        $icon->setSize(100);
        $icon->setStyle(array(
            'padding' => 0,
            'colorSaturation' => 0.8,
            'grayscaleSaturation' => 0.5,
            'colorLightness' => array(0.2, 0.8),
            'grayscaleLightness' => array(0.2, 0.8),
        ));
        $avatar = $icon->getImageData('svg');
        Storage::disk('public')->put($user->path_avatar = "avatars/$user->id.svg", $avatar);
        $user->save();
    }
}
