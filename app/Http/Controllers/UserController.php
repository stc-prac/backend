<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\SearchUserRequest;
use App\Http\Requests\User\UpdateAvatarRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\ShortUserResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;

/**
 * @group User
 */
class UserController extends Controller
{
    /**
     * Update user
     *
     * @authenticated
     *
     * @param UpdateUserRequest $request
     * @return UserResource
     */
    public function update(UpdateUserRequest $request)
    {
        $user = Auth::user();
        $user->update($request->validated());

        return new UserResource($user);
    }

    /**
     * Update avatar
     *
     * @authenticated
     *
     * @param UpdateAvatarRequest $request
     * @return UserResource
     */
    public function updateAvatar(UpdateAvatarRequest $request)
    {
        $avatar = $request->file('avatar');
        $user = Auth::user();
        $user->path_avatar = Storage::disk('public')->putFileAs("avatars", $avatar, $user->id . "." . $avatar->extension());
        $user->save();
        return new UserResource(Auth::user());
    }

    /**
     * Delete user
     *
     * @authenticated
     *
     * @return JsonResponse
     */
    public function delete()
    {
        $user = Auth::user();
        $user->delete();
        Redis::subscribe();

        return response()->json([
            'user_id' => $user->id,
            'message' => 'deleted'
        ]);
    }

    /**
     * User's info
     *
     * @authenticated
     *
     * @return UserResource
     */

    public function info(): UserResource
    {
        return new UserResource(Auth::user());
    }

    /**
     * Search user
     *
     * @authenticated
     *
     * @param $request
     * @return AnonymousResourceCollection
     */
    public function search(SearchUserRequest $request): AnonymousResourceCollection
    {
        $users = User::where('phone', 'ILIKE', $request->validated()['phone'].'%')->get(['id', 'name', 'surname','path_avatar']);

        return ShortUserResource::collection($users);
    }
}
