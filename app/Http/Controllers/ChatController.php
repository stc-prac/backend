<?php

namespace App\Http\Controllers;

use App\Events\MessageEvent;
use App\Http\Requests\Chat\SendChatMessageRequest;
use App\Http\Resources\ChatResource;
use App\Http\Resources\MessageResource;
use App\Http\Resources\ShortChatResource;
use App\Models\Chat;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

/**
 * @group Chat
 */
class ChatController extends Controller
{
    /**
     * Send message
     *
     * @authenticated
     *
     * @param SendChatMessageRequest $request
     * @param Chat $chat
     * @return MessageResource
     */
    public function sendMessage(SendChatMessageRequest $request, Chat $chat)
    {
        $user = Auth::user();
        $message = $user->messages()->make($request->validated())->toArray();
        $message = $chat->messages()->create($message);
        MessageEvent::dispatch($message);

        return new MessageResource($message);
    }

    /**
     * Get chat
     *
     * @authenticated
     *
     * @param Chat $chat
     * @return ChatResource
     */
    public function getChat(Chat $chat)
    {
        return new ChatResource($chat);
    }

    /**
     * Get all user's chats
     *
     * @authenticated
     *
     * @return AnonymousResourceCollection
     */
    public function getAllUserChats()
    {
        $user = Auth::user();

        return ShortChatResource::collection($user->chats);
    }

    public function startChat(User $user)
    {
        if (($user->chats()->pluck('id')->intersect(Auth::user()->chats()->pluck('id')))->isNotEmpty()) {
            return response()->json([
                'error' => 'чат уже создан'
            ], 422);
        }

        $chat = Chat::create();
        $chat->users()->attach(collect([$user->id, Auth::user()->id]));
        return new ChatResource($chat);
    }
}
