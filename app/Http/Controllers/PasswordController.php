<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\ForgotPasswordRequest;
use App\Http\Requests\User\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Password;

/**
 * @group Password
 *
 */
class PasswordController extends Controller
{
    /**
     * Forgot password
     *
     * @response 200 {
     * "message": "Email sent to test5@example.com"
     * }
     *
     * @response 401 {
     * "message": "Invalid email"
     * }
     *
     * @param ForgotPasswordRequest $request
     * @return JsonResponse
     */

    public function forgot(ForgotPasswordRequest $request): JsonResponse
    {
        if (!User::whereEmail($request->email)->first()->hasVerifiedEmail()) {
            return response()->json([
                'message' => "Email sent to $request->email b"
            ]);
        }
        $response = Password::sendResetLink($request->validated());

        return $response === Password::RESET_LINK_SENT
            ? response()->json(['message' => "Email sent to $request->email"], 200)
            : response()->json(['message' => "Invalid email"], 401);
    }

    /**
     * Reset password
     *
     * @response 200 {"message":"Successfully reset password"}
     * @response 401 {"error":"error"}
     *
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function reset(ResetPasswordRequest $request): JsonResponse
    {
        $status = Password::reset(
            $request->validated(),
            function ($user, $password) use ($request) {
                $user->update([
                    'password' => $password
                ]);
                event(new PasswordReset($user));
            }
        );

        return $status == Password::PASSWORD_RESET
            ? response()->json(['message' => "Successfully reset password"], 200)
            : response()->json(['error' => $status], 401);
    }
}
