<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

/**
 * @group User
 */
class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->only('register');
    }

    /**
     * Register
     *
     * @param RegisterRequest $request
     * @return UserResource
     */
    public function register(RegisterRequest $request)
    {
        $user = User::create($request->validated());
        event(new Registered($user));

        return new UserResource($user);
    }
}
