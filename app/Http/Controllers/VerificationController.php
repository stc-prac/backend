<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\ResendEmailVerificationRequest;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

/**
 * @group Verification
 *
 */
class VerificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->only('resend');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    /**
     * Verify email
     *
     * Email verification route
     *
     * @urlParam expires integer required Example: 1581112223
     * @urlParam signature string required Example: 9c83b2dc2c6a6fc2bcbe57c944ce83d9568d33f1e00d60360f89194382313e6c
     *
     * @response {"message":"Email was verified"}
     * @param Request $request
     * @return Application|JsonResponse|RedirectResponse|Redirector
     */
    public function verify(Request $request)
    {
        if (!$user = User::find($request->route('id'))) {
            return response()->json([
                'error' => 'Forbidden'
            ], 403);
        }

        if (!hash_equals((string)$request->route('hash'),
            sha1($user->getEmailForVerification()))) {
            response()->json([
                'error' => 'Forbidden'
            ], 403);
        }
        $user->markEmailAsVerified();
        event(new Verified($user));

        $url = Config::get('front.domain') . Config::get('front.email_confirmed');
        return Redirect::away($url);
    }

    /**
     * Resend
     *
     * @authenticated
     * @response {"message":"Mail was sent"}
     * @param ResendEmailVerificationRequest $request
     * @return JsonResponse
     */

    public function resend(ResendEmailVerificationRequest $request): JsonResponse
    {
        $user = Auth::user();
        $user->update($request->validated());
        $user->sendEmailVerificationNotification();
        return response()->json([
            'message' => 'Mail was sent'
        ]);
    }

    /**
     * Notice
     *
     * @authenticated
     * @return JsonResponse
     */

    public function notice(): JsonResponse
    {
        return response()->json([
            'alert' => 'Verify your email'
        ], 403);
    }
}
