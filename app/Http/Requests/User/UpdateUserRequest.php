<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'min:2', 'max:25'],
            'surname' => ['sometimes', 'required', 'min:2', 'max:50'],
            'phone' => ['sometimes', 'required', 'numeric', 'regex:/^(\+7)[489][0-9]{2}[0-9]{3}[0-9]{2}[0-9]{2}/', 'unique:users'],
            'email' => ['sometimes', 'required', 'email', 'unique:users'],
            'password' => ['sometimes','required', Password::min(6)->mixedCase()->numbers()],
        ];
    }
}
