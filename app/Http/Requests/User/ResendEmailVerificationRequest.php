<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ResendEmailVerificationRequest
 * @package App\Http\Requests\Verification
 */
class ResendEmailVerificationRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !$this->user()->hasVerifiedEmail();
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required','sometimes','email','unique:users']
        ];
    }
}
