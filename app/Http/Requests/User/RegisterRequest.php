<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:2', 'max:25'],
            'surname' => ['required', 'string', 'min:2', 'max:50'],
            'phone' => ['required', 'numeric', 'regex:/^(\+7)[489][0-9]{2}[0-9]{3}[0-9]{2}[0-9]{2}/','unique:users'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', Password::min(6)->mixedCase()->numbers()],
        ];
    }
}
