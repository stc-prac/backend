<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ShortChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $user_id = $request->user()->id;
        return [
            'chat_id' => $this->id,
            'last_message' => new MessageResource($this->messages()->orderBy('created_at', 'desc')->first()),
            'chat_user' => new ShortUserResource($this->users()->where('id', '<>', $user_id)->select(['id', 'name', 'surname','path_avatar'])->first()),
            'unread_counter' => $this->messages()->where('user_id', '=', $user_id)->where('state', '=', false)->count()
        ];
    }
}
