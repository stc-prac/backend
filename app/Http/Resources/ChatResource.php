<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'chat_id' => $this->id,
            'messages' => MessageResource::collection($this->messages()->orderBy('created_at', 'desc')->get()),
            'chat_user' => new ShortUserResource($this->users()->where('id', '<>', $request->user()->id)->select(['id', 'name', 'surname','path_avatar'])->first()),
        ];
    }
}
