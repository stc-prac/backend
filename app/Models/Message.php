<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'message',
        'state',
        'chat_id',
        'user_id'
    ];

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class, "chat_id");
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "id");
    }
}
