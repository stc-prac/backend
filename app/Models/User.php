<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $fillable = [
        'name',
        'surname',
        'phone',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'path_avatar',
        'pivot'
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function chats(): BelongsToMany
    {
        return $this->belongsToMany(Chat::class, "chat_users");
    }

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, "user_id");
    }

    public function getAvatarAttribute()
    {
        return Storage::disk('public')->url($this->path_avatar);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
}
