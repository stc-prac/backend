<?php


return [
    'domain' => env('FRONT_URL', 'http://localhost'),
    'email_confirmed' => '/login',
    'password_reset' => '/new-password'
];
