<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VerificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
\Illuminate\Support\Facades\Broadcast::routes();

Route::prefix('auth')->as('user.')->group(function () {
    Route::post('register', [RegisterController::class, 'register'])->name('register');
    Route::post('login', [LoginController::class, 'login'])->name('login');//->middleware('verified');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');
});

Route::prefix('verification')->as('verification.')->group(function () {
    Route::post('resend', [VerificationController::class, 'resend'])->name('resend');
    Route::get('verify/{id}/{hash}', [VerificationController::class, 'verify'])->name('verify');
    Route::get('notice', [VerificationController::class, 'notice'])->name('notice');
});

Route::prefix('password')->as('password.')->group(function () {
    Route::post('forgot', [PasswordController::class, 'forgot'])->name('forgot');
    Route::post('reset', [PasswordController::class, 'reset'])->name('reset');
});

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('info', [UserController::class, 'info']);
    Route::put('info', [UserController::class, 'update']);
    Route::post('avatar', [UserController::class, 'updateAvatar']);
    Route::post('search', [UserController::class, 'search']);

    Route::get('chats', [\App\Http\Controllers\ChatController::class, 'getAllUserChats']);
    Route::get('chat/{chat}', [\App\Http\Controllers\ChatController::class, 'getChat']);
    Route::post('chat/{user}/start', [\App\Http\Controllers\ChatController::class, 'startChat']);
    Route::post('chat/{chat}/send', [\App\Http\Controllers\ChatController::class, 'sendMessage']);
});

