<?php

namespace Database\Seeders;

use App\Models\Chat;
use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        $users = User::factory()->count(10)->create();
        foreach ($users as $user) {
            $anotherUser = $users->except($user->id)->random();
            $users->forget([$user->id, $anotherUser->id]);
            $chat = $user->chats()->create();
            $chat->users()->attach($anotherUser);
            for ($i = 0; $i < random_int(2, 20); $i++) {
                $message = Message::factory()->for($chat)->make()->toArray();
                $user->messages()->create($message);
                $message = Message::factory()->for($chat)->make()->toArray();
                $anotherUser->messages()->create($message);
            }
        }
    }
}
