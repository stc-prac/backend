<?php

namespace Tests\Feature\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Notifications\VerifyEmail;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Notification;

class VerificationTest extends TestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->unverified()->create();
    }

    public function testSuccess()
    {
        Notification::fake();
        $user = $this->user;
        event(new Registered($user));
        $this->assertFalse($this->user->hasVerifiedEmail());
        Notification::assertSentTo(
            $user, VerifyEmail::class,
            function ($notification) use ($user) {
                $mail = $notification->toMail($user)->toArray();
                $this->get($mail['actionUrl']);
                $user = $user->refresh();
                return $user->hasVerifiedEmail();
            }
        );
        $this->assertTrue($this->user->hasVerifiedEmail());
    }
}

