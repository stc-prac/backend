<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Tests\TestCase;

class PasswordTest extends TestCase
{
    use WithFaker;

    protected $phone, $password, $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->password = 'passWord9';
        $this->phone = $this->faker->phoneNumber;
        $this->user = User::factory()->create([
            'phone' => $this->phone,
            'password' => $this->password
        ]);
    }

    public function testSuccess()
    {
        Notification::fake();
        $data = [
            'email' => $this->user->email
        ];
        $response = $this->postJson(route('password.forgot'), $data);
        $response->assertOk();

        $user = $this->user;
        Notification::assertSentTo(
            $user, ResetPassword::class,
            function ($notification) use ($user) {
                $mail = $notification->toMail($user)->toArray();
                $url = parse_url($mail['actionUrl']);
                $this->assertEquals(Config::get('front.url'), $url['scheme'] . '://' . $url['host']);
                parse_str($url['query'], $params);
                $data = [
                    'token' => $params['token'],
                    'email' => $params['email'],
                    'password' => 'passWord99'
                ];
                $this->postJson(route('password.reset'), $data)->assertOk();

                $data = [
                    'phone' => $user->phone,
                    'password' => $data['password']
                ];
                $response = $this->postJson(route('user.login'), $data);
                return $response->assertOk();
            }
        );
        $data = [
            'phone' => $this->phone,
            'password' => $this->password
        ];
        $this->postJson(route('user.login'), $data)->assertUnauthorized();//403
    }

    /**
     * @bugtest
     */
    public function testEmailNotVerified()
    {
        Notification::fake();
        $data = [
            'email' => $this->user->email
        ];
        $this->user->forceFill(['email_verified_at' => null]);
        $this->user->save();
        $this->postJson(route('password.forgot'), $data)->assertOk();
        Notification::assertNothingSent();
    }

    /**
     * @bugtest
     */
    public function testEmailNotInDB()
    {
        Notification::fake();
        $data = [
            'email' => $this->faker->safeEmail
        ];
        $this->postJson(route('password.forgot'), $data)->assertJsonValidationErrors([
            'email'
        ]);
        Notification::assertNothingSent();
    }

    public function testInvalidToken()
    {
        Notification::fake();
        $data = [
            'email' => $this->user->email
        ];
        $this->postJson(route('password.forgot'), $data)->assertOk();

        $data = [
            'token' => Str::random(),
            'email' => $this->user->email,
            'password' => 'passWord9'
        ];
        $this->postJson(route('password.reset'), $data)->assertUnauthorized();

        $data = [
            'phone' => $this->user->phone,
            'password' => $this->password
        ];
        $this->postJson(route('user.login'), $data)->assertOk();
    }
}
