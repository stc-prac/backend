<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ResendVerificationTest extends TestCase
{
    use WithFaker;

    protected $email, $password, $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->password = Str::random(8);
        $this->email = $this->faker->unique()->safeEmail;
        $this->user = User::factory()->unverified()->create([
            'email' => $this->email,
            'password' => $this->password
        ]);
    }

    public function testCurrentEmail()
    {
        Notification::fake();
        $this->assertFalse($this->user->hasVerifiedEmail());
        $data = [
        ];
        Sanctum::actingAs($this->user);
        $this->postJson(route('verification.resend'), $data)->assertOk();
        Notification::assertSentTo($this->user, VerifyEmail::class);
    }

    public function testEmailAlreadyVerified()
    {
        Notification::fake();
        $this->user->markEmailAsVerified();
        $data = [
        ];
        Sanctum::actingAs($this->user);
        $this->postJson(route('verification.resend'), $data)->assertForbidden();
        Notification::assertNothingSent();
    }

    public function testUnauthorized()
    {
        Notification::fake();
        $this->postJson(route('verification.resend'))->assertUnauthorized();
    }
}
