<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AvatarTest extends TestCase
{
    protected $user, $password;
    protected function setUp(): void
    {
        parent::setUp();
        $this->password = 'passWord9';
        $this->user = User::factory()->create([
            'email' => $this->faker->unique()->safeEmail(),
            'password' => $this->password
        ]);
    }

    public function testSuccess()
    {
        Storage::fake('public');
        Auth::login($this->user);
        $data = [
            'avatar' => UploadedFile::fake()->image('avatar.png')
        ];
        $this->postJson(route('avatar'), $data)->assertCreated();
        $avatar = $this->user->path_avatar;
        Storage::disk('public')->assertExists($avatar);
    }

    public function testInvalidFile()
    {
        Auth::login($this->user);
        $data = [
            'avatar' => UploadedFile::fake()->image('avatar.pdf', '650', '700')
        ];
        $this->postJson(route('avatar'), $data)->assertJsonValidationErrors('avatar');
    }

    public function testUnauthorized()
    {
        $data = [
            'avatar' => UploadedFile::fake()->image('avatar.png')
        ];
        $this->postJson(route('avatar'), $data)->assertUnauthorized();
    }
}