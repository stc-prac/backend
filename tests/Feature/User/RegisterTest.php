<?php

namespace Tests\Feature\User;

use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function testSuccess()
    {
        Event::fake();
        $data = [
            'name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'phone' => '+79219876543',
            'email' => $this->faker->unique()->safeEmail(),
            'password' => 'Password1'
        ];

        $this->postJson(route('user.register'), $data)->dump()->assertCreated();
        Event::assertDispatched(Registered::class);
    }

    public function testUnregistered()
    {
        Event::fake();
        $data = [
            'name' => 12345,
            'surname' => true,
            'phone' => '+792198ghhgg',
            'email' => 'dfgdfgghg',
            'password' => false
        ];

        $this->postJson(route('user.register'), $data)->assertJsonValidationErrors([
            'name',
            'surname',
            'phone',
            'email',
            'password'
        ]);
        Event::assertNotDispatched(Registered::class);
    }
}
