<?php

namespace Tests\Feature\User;

use Tests\TestCase;

class LoginTest extends TestCase
{
    protected $user, $password;

    protected function setUp(): void
    {
        parent::setUp();
        $this->password = 'passWord9';
        $this->user = \App\Models\User::factory()->create([
            'password' => $this->password
        ]);
    }

    public function testSuccess()
    {
        $data = [
            'phone' => $this->user->phone,
            'password' => $this->password
        ];

        $this->postJson(route('user.login'), $data)->assertOk();
    }

    public function testUnauthorized()
    {
        $data = [
            'phone' => 'trtt',
            'password' => $this->password
        ];

        $this->postJson(route('user.login'), $data)->assertUnauthorized();

        $data = [
            'phone' => $this->user->phone,
            'password' => 'fgdgdf'
        ];

        $this->postJson(route('user.login'), $data)->assertUnauthorized();
    }

    public function testValidation()
    {
        $data = [
            'phone' => '',
        ];

        $this->postJson(route('user.login'), $data)->assertJsonValidationErrors([
            'phone',
            'password'
        ]);

        $data = [
            'phone' => 123,
            'password' => true
        ];

        $this->postJson(route('user.login'), $data)->assertJsonValidationErrors([
            'phone',
            'password'
        ]);
    }

}